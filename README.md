How to run this apps using docker
run this command sequentially on your command line
1. docker build -t docker-php-tes .
2. docker-compose up -d
3. access in browser http://127.0.0.1:8004/, boxbilling landing page on browser will appear
4. to access phpmyadmin you can write http://127.0.0.1:5050/ in browser using username: "root" and password: "password"
