<?php
$boxbilling_db = file_get_contents("./boxbilling-json/boxbilling_db.json");
$boxbilling_db = json_decode($boxbilling_db);
?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Materialize icons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- Styling CSS -->
    <link rel="stylesheet" href="styles.css">


    <title>Niagahoster</title>
</head>

<body>
    <header id="header-web">
        <div class="container logo-menu">
            <div class="row">
                <div class="col-xs-4 col-md-4">
                    <a href="https://www.niagahoster.co.id/"><img src="https://www.niagahoster.co.id/assets/images/2018/logo.svg" class="img-responsive logo-desktop hd-320-719 hd-720-1023" alt="niagahoster"></a>
                </div>
                <div class="col-xs-8 col-md-8 header-menu-right">
                    <ul>
                        <li><a class="text-decoration-none p-2 nunito-regular"><i class="fa fa-phone"></i> 0274-2885822</a></li>
                        <li><a class="text-decoration-none p-2 nunito-regular"><i class="fa fa-comments"></i> Live Chat</a></li>
                        <li class="btn btn-outline-dark btn-header-right nunito-regular"><i class="fa fa-user"></i> Login</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container-fluid nav-outline">
            <div class="row">
                <div class="col-12">
                    <nav class="navbar navbar-expand-md navbar-fixed-top navbar-light bg-light main-nav navbar-niagahoster">
                        <div class="container">
                            <ul class="nav navbar-nav mx-auto">
                                <li class="nav-item"><a class="nav-link" href="#">Website Hosting Indonesia</a></li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Domain Murah
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="#">Beli Domain Murah</a>
                                        <a class="dropdown-item" href="#">Transfer Domain</a>
                                </li>
                                <li class="nav-item"><a class="nav-link" href="#">Domain Murah</a></li>
                                <li class="nav-item"><a class="nav-link" href="#">Cloud VPS</a></li>
                                <li class="nav-item"><a class="nav-link" href="#">Cloud Hosting</a></li>
                                <li class="nav-item"><a class="nav-link" href="#">Afiliasi</a></li>
                                <li class="nav-item"><a class="nav-link" href="#">Promo</a></li>
                                <li class="nav-item"><a class="nav-link" href="#">Buat Website</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>

    <header id="header-mobile">
        <div class="nav">
            <input type="checkbox" id="nav-check">
            <div class="nav-header">
                <div class="nav-title">
                    <a href=""><img src="./assets/logo-mobile.svg" alt=""></a>
                </div>
            </div>
            <div class="header-menu-right">
                <ul>
                    <li id="fa-phone"><a class="text-decoration-none p-2 nunito-regular"><i class="fa fa-phone"></i> 0274-2885822</a></li>
                    <li id="fa-comments"><a class="text-decoration-none p-2 nunito-regular"><i class="fa fa-comments"></i> Live Chat</a></li>
                    <li class="btn btn-outline-dark btn-header-right nunito-regular"><i class="fa fa-user"></i> Login</a></li>
                </ul>
            </div>
            <div class="nav-btn" id="nav-btn">
                <label for="nav-check">
                    <i class="fa fa-bars"></i>
                </label>
            </div>
            <div id="dropdown-nav" style="display:none;">
                <ul>
                    <li><a href=""><i class="fa fa-phone"></i> 0274-2885822</a><a href="">      <i class="fa fa-comments"></i> Live Chat</a></li>
                    <li><a href="">Web Hosting Indonesia</a></li>
                    <li id="dropdown-inside" style="padding-left:15px;" >Domain Murah <i class="fa fa-caret-down"></i></li>
                        <ul id="dropdown-inside-menu" style="display:none;">
                            <li><a href="">Beli Domain Murah</a></li>
                            <li><a href="">Transfer Domain</a></li>
                        </ul>
                    <li><a href="">Cloud VPS</a></li>
                    <li><a href="">Cloud Hosting</a></li>
                    <li><a href="">Afiliasi</a></li>
                    <li><a href="">Promo</a></li>
                    <li><a href="">Buat Website</a></li>
                </ul>
            </div>
        </div>
    </header>

    <!--SECTION 1-->
    <section class="content-center" style="margin-top: 25px;">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <h1 class="title-hosting">PHP HOSTING</h1>
                    <h3 class="title-content">Cepat, handal, penuh dengan modul php yang anda butuhkan</h3>
                    <ul class="title-list">
                        <li><i class="fa fa-check-circle"></i> Solusi PHP untuk performa query yang lebih cepat</li>
                        <li><i class="fa fa-check-circle"></i> Konsumsi memori yang lebih rendah</li>
                        <li><i class="fa fa-check-circle"></i> Support PHP 5.3, PHP 5.4, PHP 5.5 PHP 5.6, PHP 7</li>
                        <li><i class="fa fa-check-circle"></i> Fitur enkripsi IonCube dna Zend Guard Loaders</li>
                    </ul>
                </div>
                <div class="col-md-6 d-none d-sm-block banner-hosting-section-1">
                    <img src="./assets/illustration-banner-PHP-hosting-01.svg" alt="">
                </div>
            </div>
        </div>
    </section>
    <!--SECTION 1-->

    <!--SECTION 2-->
    <section class="content-center-no-border">
        <div class="container">
            <div class="row d-flex justify-content-center align-items-center">
                <div class="col-md-3 col-sm-12 text-center">
                    <embed width="163" height="142" src="./assets/illustration-banner-PHP-zenguard01.svg">
                    <h4 class="pt-2 title-image-section-2">PHP Zend Guard Loader</h4>
                </div>
                <div class="col-md-3 col-sm-12 text-center">
                    <embed width="163" height="142" src="./assets/icon-composer.svg">
                    <h4 class="pt-2 title-image-section-2">PHP Composer</h4>
                </div>
                <div class="col-md-3 col-sm-12 text-center">
                    <embed width="163" height="142" src="./assets/icon-php-hosting-ioncube.svg">
                    <h4 class="pt-2 title-image-section-2">PHP ionCube Loader</h4>
                </div>
            </div>
        </div>
    </section>
    <!--SECTION 2-->

    <!--SECTION 3-->
    <section class="content-center-no-border">
        <div class="container">
            <div class="row d-flex justify-content-center align-items-center">
                <div class="col-12 col-sm-12 text-center">
                    <h3 class="pt-2 title-hosting-section3">Paket Hosting Singapura yang Tepat</h3>
                    <h4 class="pt-2 title-hosting-section3-content">Diskon 40% + Domain dan SSL Gratis Untuk Anda</h4>
                </div>
            </div>
        </div>
    </section>
    <!--SECTION 3-->

    <!--SECTION 4-->
    <section class="content-center-no-border">
        <div class="container">
            <div class="row d-flex">
                <div class="col-md-3 col-sm-12 text-center">
                    <div class="table-section4-price-package">
                        <p class="section4-table-title"><?php echo $boxbilling_db->{0}->package ?></p>
                    </div>
                    <div class="table-section4-price-package">
                        <p class="section4-table-price-normal"><span class="rp-little">Rp </span><?php echo $boxbilling_db->{0}->price ?></p><br>
                        <?php
                        $price = explode(".", $boxbilling_db->{0}->price_discount);
                        ?>
                        <p><span class="rp-bigger">Rp </span><span class="section4-table-price-discount-front"><?php echo $price[0]; ?></span><span class="section4-table-price-discount-back">.<?php echo $price[1]; ?><span class="month-package">/bln</span></span></p>
                    </div>
                    <div class="table-section4-price-package">
                        <p class="text-content-table-section4"><span class="text-bold-content-table-section4">938</span> Pengguna terdaftar</p>
                    </div>
                    <div class="table-section4-price-package">
                        <p class="text-bold-content-table-section4 pt-2">0.5X RESOURCE POWER</p>
                        <p class="text-content-table-section4"><span class="text-bold-content-table-section4">Unlimited</span> Bandwidth</p>
                        <p class="text-content-table-section4"><span class="text-bold-content-table-section4">Unlimited</span> Databases</p>
                        <p class="text-content-table-section4"><span class="text-bold-content-table-section4">1</span> Domain</p>
                        <p class="text-content-table-section4"><span class="text-bold-content-table-section4">Instant</span> Backup</p>
                        <p class="text-content-table-section4"><span class="text-bold-content-table-section4">Unlimited SSL</span> Gratis Selamanya</p>
                        <button class="button-white mb-4">Pilih Sekarang</button>
                    </div>

                </div>
                <div class="col-md-3 col-sm-12 text-center">
                    <div class="table-section4-price-package">
                        <p class="section4-table-title"><?php echo $boxbilling_db->{1}->package ?></p>
                    </div>
                    <div class="table-section4-price-package">
                        <p class="section4-table-price-normal"><span class="rp-little">Rp </span><?php echo $boxbilling_db->{1}->price ?></p><br>
                        <?php
                        $price = explode(".", $boxbilling_db->{1}->price_discount);
                        ?>
                        <p><span class="rp-bigger">Rp </span><span class="section4-table-price-discount-front"><?php echo $price[0]; ?></span><span class="section4-table-price-discount-back">.<?php echo $price[1]; ?><span class="month-package">/bln</span></span></p>
                    </div>
                    <div class="table-section4-price-package">
                        <p class="text-content-table-section4"><span class="text-bold-content-table-section4">4168</span> Pengguna terdaftar</p>
                    </div>
                    <div class="table-section4-price-package">
                        <p class="text-bold-content-table-section4 pt-2">1X RESOURCE POWER</p>
                        <p class="text-content-table-section4"><span class="text-bold-content-table-section4">Unlimited</span> Disk Space</p>
                        <p class="text-content-table-section4"><span class="text-bold-content-table-section4">Unlimited</span> Bandwith</p>
                        <p class="text-content-table-section4"><span class="text-bold-content-table-section4">Unlimited</span> POP3 Email</p>
                        <p class="text-content-table-section4"><span class="text-bold-content-table-section4">Unlimited</span> Databases</p>
                        <p class="text-content-table-section4"><span class="text-bold-content-table-section4">10</span> Addon Domain</p>
                        <p class="text-content-table-section4"><span class="text-bold-content-table-section4">Instant</span> Backup</p>
                        <p class="text-content-table-section4"><span class="text-bold-content-table-section4">Domain Gratis</span> Gratis Selamanya</p>
                        <p class="text-content-table-section4"><span class="text-bold-content-table-section4">Unlimited SSL</span> Gratis Selamanya</p>
                        <button class="button-white mb-4">Pilih Sekarang</button>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12 text-center">
                    <div class="table-section4-price-package-blue-first">
                        <div id="myDiv">
                            <h5>BEST SELLER!</h5>
                        </div>
                        <p class="section4-table-title-white"><?php echo $boxbilling_db->{2}->package ?></p>
                    </div>
                    <div class="table-section4-price-package-blue-first">
                        <p class="section4-table-price-normal-white"><span class="rp-little-white">Rp </span><?php echo $boxbilling_db->{2}->price ?></p><br>
                        <?php
                        $price = explode(".", $boxbilling_db->{2}->price_discount);
                        ?>
                        <p><span class="rp-bigger-white">Rp </span><span class="section4-table-price-discount-front-white"><?php echo $price[0]; ?></span><span class="section4-table-price-discount-back-white">.<?php echo $price[1]; ?><span class="month-package">/bln</span></span></p>
                    </div>
                    <div class="table-section4-price-package-blue-second">
                        <p class="text-content-table-section4-white"><span class="text-bold-content-table-section4-white">10.017</span> Pengguna terdaftar</p>
                    </div>
                    <div class="table-section4-price-package-blue">
                        <p class="text-bold-content-table-section4 pt-2">2X RESOURCE POWER</p>
                        <p class="text-content-table-section4"><span class="text-bold-content-table-section4">Unlimited</span> Disk Space</p>
                        <p class="text-content-table-section4"><span class="text-bold-content-table-section4">Unlimited</span> Bandwith</p>
                        <p class="text-content-table-section4"><span class="text-bold-content-table-section4">Unlimited</span> POP3 Email</p>
                        <p class="text-content-table-section4"><span class="text-bold-content-table-section4">Unlimited</span> Databases</p>
                        <p class="text-content-table-section4"><span class="text-bold-content-table-section4">Unlimited</span> Addon Domain</p>
                        <p class="text-content-table-section4"><span class="text-bold-content-table-section4">Instant</span> Backup</p>
                        <p class="text-content-table-section4"><span class="text-bold-content-table-section4">Domain Gratis</span> Gratis Selamanya</p>
                        <p class="text-content-table-section4"><span class="text-bold-content-table-section4">Unlimited SSL</span> Gratis Selamanya</p>
                        <p class="text-content-table-section4"><span class="text-bold-content-table-section4">Private </span> Name Server</p>
                        <p class="text-content-table-section4"><span class="text-bold-content-table-section4">SpamAssasin </span> Mail Protection</p>
                        <button class="button-blue mb-4">Pilih Sekarang</button>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12 text-center">
                    <div class="table-section4-price-package">
                        <p class="section4-table-title"><?php echo $boxbilling_db->{3}->package ?></p>
                    </div>
                    <div class="table-section4-price-package">
                        <p class="section4-table-price-normal"><span class="rp-little">Rp </span><?php echo $boxbilling_db->{3}->price ?></p><br>
                        <?php
                        $price = explode(".", $boxbilling_db->{3}->price_discount);
                        ?>
                        <p><span class="rp-bigger">Rp </span><span class="section4-table-price-discount-front"><?php echo $price[0]; ?></span><span class="section4-table-price-discount-back">.<?php echo $price[1]; ?><span class="month-package">/bln</span></span></p>
                    </div>
                    <div class="table-section4-price-package">
                        <p class="text-content-table-section4"><span class="text-bold-content-table-section4">3.552</span> Pengguna terdaftar</p>
                    </div>
                    <div class="table-section4-price-package">
                        <p class="text-bold-content-table-section4 pt-2">2X RESOURCE POWER</p>
                        <p class="text-content-table-section4"><span class="text-bold-content-table-section4">Unlimited</span> Disk Space</p>
                        <p class="text-content-table-section4"><span class="text-bold-content-table-section4">Unlimited</span> Bandwith</p>
                        <p class="text-content-table-section4"><span class="text-bold-content-table-section4">Unlimited</span> POP3 Email</p>
                        <p class="text-content-table-section4"><span class="text-bold-content-table-section4">Unlimited</span> Databases</p>
                        <p class="text-content-table-section4"><span class="text-bold-content-table-section4">Unlimited</span> Addon Domain</p>
                        <p class="text-content-table-section4"><span class="text-bold-content-table-section4">Magic auto</span> Backup</p>
                        <p class="text-content-table-section4"><span class="text-bold-content-table-section4">Domain Gratis</span> Gratis Selamanya</p>
                        <p class="text-content-table-section4"><span class="text-bold-content-table-section4">Unlimited SSL</span> Gratis Selamanya</p>
                        <p class="text-content-table-section4"><span class="text-bold-content-table-section4">Private </span> Name Server</p>
                        <p class="text-content-table-section4"><span class="text-bold-content-table-section4">SpamExpert </span> Pro Mail Protection</p>
                        <button class="button-white mb-4">Diskon 40%</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--SECTION 4-->

    <!--SECTION 5-->
    <section class="content-center-no-border" id="box-section5-large">
        <div class="container">
            <div class="row d-flex justify-content-center align-items-center">
                <div class="col-12 col-sm-12 text-center">
                    <h4 class="pt-2 title-hosting-section5-section6-section7-section8">Powerful dengan Limit PHP yang Lebih Besar</h4>
                </div>
            </div>
            <div class="row d-flex justify-content-center align-items-center">
                <div class="col-md-3 col-sm-12">

                </div>
                <div class="col-md-3 col-sm-6 d-flex justify-content-center">
                    <table class="table-section5">
                        <tbody>
                            <tr>
                                <th class="text-left" style="padding:0 5px;width:54px;"><i class="fa fa-check-circle"></i></th>
                                <th class="text-center"><span class=""> max execution 300s<span></i></th>
                            </tr>
                            <tr>
                                <th class="text-left" style="padding:0 5px;width:54px;"><i class="fa fa-check-circle"></i></th>
                                <th class="text-center"><span class=""> max execution 300s<span></i></th>
                            </tr>
                            <tr>
                                <th class="text-left" style="padding:0 5px;width:54px;"><i class="fa fa-check-circle"></i></th>
                                <th class="text-center"><span class=""> php memory limit 1024mb<span></i></th>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-3 col-sm-6 d-flex justify-content-center">
                    <table class="table-section5">
                        <tbody>
                            <tr>
                                <th class="text-left" style="padding:0 5px;width:54px;"><i class="fa fa-check-circle"></i></th>
                                <th class="text-center"><span class=""> post max size 128MB<span></i></th>
                            </tr>
                            <tr>
                                <th class="text-left" style="padding:0 5px;width:54px;"><i class="fa fa-check-circle"></i></th>
                                <th class="text-center"><span class=""> upload max filesize 128MB<span></i></th>
                            </tr>
                            <tr>
                                <th class="text-left" style="padding:0 5px;width:54px;"><i class="fa fa-check-circle"></i></th>
                                <th class="text-center"><span class=""> max input vars 2500<span></i></th>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-3 col-sm-">

                </div>
            </div>
            <div class="row d-flex justify-content-center align-items-center">
                <div class="col-md-4 col-sm-12 text-center">

                </div>
                <div class="col-md-4 col-sm-12 text-center">
                    <div class="d-flex justify-content-center">
                        <div class="little-bottom-border ">

                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 text-center ">

                </div>
            </div>
        </div>
    </section>


    <section class="content-center-no-border" id="box-section5-mobile">
        <div class="container">
            <div class="row d-flex justify-content-center align-items-center">
                <div class="col-12 col-sm-12 text-center">
                    <h4 class="pt-2 title-hosting-section5-section6-section7-section8">Powerful dengan Limit PHP yang Lebih Besar</h4>
                </div>
            </div>
            <div class="row d-flex justify-content-center align-items-center">
                <div class="col-sm-6 col-12 d-flex justify-content-center table-section5-first">
                    <table class="table-section5">
                        <tbody>
                            <tr>
                                <th class="text-left" style="padding:0 5px;width:54px;"><i class="fa fa-check-circle"></i></th>
                                <th class="text-center"><span class=""> max execution 300s<span></i></th>
                            </tr>
                            <tr>
                                <th class="text-left" style="padding:0 5px;width:54px;"><i class="fa fa-check-circle"></i></th>
                                <th class="text-center"><span class=""> max execution 300s<span></i></th>
                            </tr>
                            <tr>
                                <th class="text-left" style="padding:0 5px;width:54px;"><i class="fa fa-check-circle"></i></th>
                                <th class="text-center"><span class=""> php memory limit 1024mb<span></i></th>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-sm-6 col-12 d-flex justify-content-center table-section5-second">
                    <table class="table-section5">
                        <tbody>
                            <tr>
                                <th class="text-left" style="padding:0 5px;width:54px;"><i class="fa fa-check-circle"></i></th>
                                <th class="text-center"><span class=""> post max size 128MB<span></i></th>
                            </tr>
                            <tr>
                                <th class="text-left" style="padding:0 5px;width:54px;"><i class="fa fa-check-circle"></i></th>
                                <th class="text-center"><span class=""> upload max filesize 128MB<span></i></th>
                            </tr>
                            <tr>
                                <th class="text-left" style="padding:0 5px;width:54px;"><i class="fa fa-check-circle"></i></th>
                                <th class="text-center"><span class=""> max input vars 2500<span></i></th>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row d-flex justify-content-center align-items-center">
                <div class="col-md-4 col-sm-12 text-center">

                </div>
                <div class="col-md-4 col-sm-12 text-center">
                    <div class="d-flex justify-content-center">
                        <div class="little-bottom-border ">

                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 text-center ">

                </div>
            </div>
        </div>
    </section>
    <!--SECTION 5-->

    <!--SECTION 6-->
    <section class="content-center-no-border">
        <div class="container">
            <div class="row d-flex justify-content-center align-items-center">
                <div class="col-12 col-sm-12 text-center">
                    <h4 class="pt-4 title-hosting-section5-section6-section7-section8">Semua Paket Hosting Sudah Termasuk</h4>
                </div>
            </div>
            <div class="row d-flex justify-content-center align-items-center">
                <div class="col-md-4 col-sm-12 text-center text-section-6">
                    <img class="image-section6" src="./assets/icon-PHP-Hosting_PHP-Semua-Versi.svg" alt="">
                    <h5 class="pt-2">PHP Semua Versi</h5>
                    <p>Pilih Mulai dari PHP 5.3 s/d PHP 7 <br>Ubah sesuka anda!</p>
                </div>
                <div class="col-md-4 col-sm-12 text-center text-section-6">
                    <img class="image-section6" src="./assets/icon-PHP-Hosting_My SQL.svg" alt="">
                    <h5 class="pt-2">MySQL Versi 5.6</h5>
                    <p>Nikmati Mysql versi terbaru, tercepat dan <br>kaya akan fitur</p>
                </div>
                <div class="col-md-4 col-sm-12 text-center text-section-6">
                    <img class="image-section6" src="./assets/icon-PHP-Hosting_CPanel.svg" alt="">
                    <h5 class="pt-2">Panel Hosting Cpanel</h5>
                    <p>Kelola website dengan panel canggih yang <br>familiar di hati Anda</p>
                </div>
            </div>
            <div class="row d-flex justify-content-center align-items-center">
                <div class="col-md-4 col-sm-12 text-center text-section-6">
                    <img class="image-section6" src="./assets/icon-PHP- Hosting_garansi uptime.svg" alt="">
                    <h5 class="pt-2 title-hosting-section5">Garansi Uptime 99.9%</h5>
                    <p>Data center yang mendukung kelangsungan <br>website Anda 24/7</p>
                </div>
                <div class="col-md-4 col-sm-12 text-center text-section-6">
                    <img class="image-section6" src="./assets/icon-PHP-Hosting_InnoDB.svg" alt="">
                    <h5 class="pt-2 title-hosting-section5">Database InnoDb Unlimited</h5>
                    <p>Jumlah dan ukuran database yang tumbuh <br>sesuai kebutuhan Anda</p>
                </div>
                <div class="col-md-4 col-sm-12 text-center text-section-6">
                    <img class="image-section6" src="./assets/icon-PHP-Hosting_My SQL-remote.svg" alt="">
                    <h5 class="pt-2 title-hosting-section5">Wildcard Remote MySQL</h5>
                    <p>Mendukung s/d 25 max_user_connections <br>dari 100 max_connections</p>
                </div>
            </div>
            <div class="row d-flex justify-content-center align-items-center">
                <div class="col-md-4 col-sm-12 text-center">

                </div>
                <div class="col-md-4 col-sm-12 text-center">
                    <div class="d-flex justify-content-center">
                        <div class="little-bottom-border ">

                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 text-center ">

                </div>
            </div>
        </div>
    </section>
    <!--SECTION 6-->

    <!--SECTION 7-->
    <section class="content-center-no-border">
        <div class="container">
            <div class="row d-flex justify-content-center align-items-center">
                <div class="col-md-12 col-sm-12 text-center">
                    <h4 class="pt-2 title-hosting-section5-section6-section7-section8">Mendukung Penuh Framework Laravel</h4>
                </div>
                <div class="col-md-6 col-sm-12 text-left">
                    <h3 class="title-content-section-7">Tak Perlu menggunakan dedicated server ataupun VPS yang mahal . Layanan PHP hosting murah kami mendukung penuh framework anda</h3>
                    <ul class="title-list-section-7">
                        <li><i class="fa fa-check-circle"></i> Install Laravel <span style="font-weight:700;">1 klik</span> dengan softaculous Installer</li>
                        <li><i class="fa fa-check-circle"></i> Mendukung ekstensi <span style="font-weight:700;">PHP MCrypt, phar, mbstring, json, dan fileinfo</span></li>
                        <li><i class="fa fa-check-circle"></i> Tersedia <span style="font-weight:700;">Composer dan SSH</span> untuk menginstall packages pilihan Anda</li>
                    </ul>
                    <p class="title-content-nb-section7">Nb Composer dan SSH hanya tersedia pada paket Personal dan Bisnis</p>
                    <button class="button-blue">Pilih Hoting Anda</button>
                </div>
                <div class="col-md-6 col-sm-12">
                    <embed class="image-section7" src="./assets/illustration-banner-support-laravel-hosting.svg">
                </div>
            </div>
        </div>
    </section>
    <!--SECTION 7-->

    <!--SECTION 8-->
    <section class="content-center-no-border" id="section8-box">
        <div class="container">
            <div class="row d-flex justify-content-center align-items-center">
                <div class="col-12 text-center">
                    <h4 class="pt-2 title-hosting-section5-section6-section7-section8">Modul Lengkap Untuk Menjalankan Aplikasi PHP Anda</h4>
                </div>
            </div>
            <div class="row d-flex justify-content-start align-items-center content-section-8">
                <div class="col-md-3  text-left">
                    <p>IcePHP
                    <p>
                    <p>apc
                    <p>
                    <p>apcu</p>
                    <p>apm</p>
                    <p>ares</p>
                    <p>bcmath</p>
                    <p>bcompiler</p>
                    <p>big_int</p>
                    <p>bitset</p>
                    <p>bloomy</p>
                    <p>bz2_filter</p>
                    <p>clamav</p>
                    <p>coin_acceptor</p>
                    <p>crack</p>
                    <p>dba</p>
                </div>
                <div class="col-md-3  text-left">
                    <p>http</p>
                    <p>huffman</p>
                    <p>idn</p>
                    <p>igbinary</p>
                    <p>imagick</p>
                    <p>imap</p>
                    <p>inclued</p>
                    <p>inotify</p>
                    <p>interbase</p>
                    <p>intl</p>
                    <p>ioncube_loader</p>
                    <p>ioncube_loader_4</p>
                    <p>jsmin</p>
                    <p>json</p>
                    <p>ldap</p>
                </div>
                <div class="col-md-3  text-left">
                    <p>nd_pdo_mysql</p>
                    <p>oauth</p>
                    <p>oci8</p>
                    <p>odbc</p>
                    <p>opcache</p>
                    <p>pdf</p>
                    <p>pdo</p>
                    <p>pdo_dblib</p>
                    <p>pdo_firebird</p>
                    <p>pdo_mysql</p>
                    <p>pdo_odbc</p>
                    <p>pdo_pgsql</p>
                    <p>pdo_sqlite</p>
                    <p>pgsql</p>
                    <p>phalcon</p>
                </div>
                <div class="col-md-3 text-left">
                    <p>stats</p>
                    <p>stem</p>
                    <p>stomp</p>
                    <p>suhosin</p>
                    <p>sybase_cl</p>
                    <p>sysvmsg</p>
                    <p>sysvsem</p>
                    <p>sysvshm</p>
                    <p>tidy</p>
                    <p>timezonedb</p>
                    <p>trader</p>
                    <p>translit</p>
                    <p>uploadprogress</p>
                    <p>uri_template</p>
                    <p>uuid</p>
                </div>
            </div>
            <div class="row d-flex justify-content-start align-items-center content-section-8">
                <div class="col-md-12 d-sm-none d-md-block text-center">
                    <button class="button-white">Selengkapnya</button>
                </div>
            </div>
        </div>
    </section>
    <!--SECTION 8-->

    <!--SECTION 9-->
    <section class="content-center-no-border">
        <div class="container">
            <div class="row d-flex justify-content-start align-items-center content-section-8">
                <div class="col-md-6 col-sm-12 text-left">
                    <h3 class="title-content-section-9-title">Linux Hosting yang Stabil <br>dengan Teknologi LVE</h3>
                    <p class="title-content-section-9">SuperMicro <span style="font-weight:700;">Intel Xeon 24-Cores server</span> dengan RAM <span style="font-weight:700;">128 GB</span> dan teknologi <span style="font-weight:700;">LVE CloudLinux</span> untuk stabilitas server Anda.Dilengkapi dengan <span style="font-weight:700;">SSD</span> untuk kecepatan <span style="font-weight:700;">MySQL</span> dan caching. Apache load balancer berbasis Litespeed Technologies, <span style="font-weight:700;">CageFS</span> security, <span style="font-weight:700;">Raind-10</span> protection dan auto backup untuk keamanan website PHP Anda </p>
                    <button class="button-blue">Pilih Hoting Anda</button>
                </div>
                <div class="col-md-6 col-sm-12 text-left">
                    <img class="image-section9" src="./assets/image-support.png" alt="">
                </div>
            </div>
        </div>
    </section>
    <!--SECTION 9-->

    <!--SECTION 10-->
    <section class="content-center-no-border-section10">
        <div class="container">
            <div class="row d-flex align-items-center box-content-section10">
                <div class=" col-lg-6 col-md-12 col-sm-12 ">
                    <h3 class="title-content-section-10-title">Bagikan jka Anda menyukai halaman ini</h3>
                </div>
                <div class=" col-lg-6 col-md-12 col-sm-12">
                    <div class="jssocials-shares box-content-socialmedia-icon-section10">
                        <div class="jssocials-share jssocials-share-twitter"><a href="#" class="jssocials-share-link"><i class="fa fa-twitter-square"></i></a>
                            <div class="jssocials-share-count-box jssocials-share-no-count"><span class="jssocials-share-count"></span></div>
                        </div>
                        <div class="jssocials-share jssocials-share-facebook"><a href="#" class="jssocials-share-link"><i class="fa fa-facebook-square"></i></a>
                            <div class="jssocials-share-count-box jssocials-share-no-count"><span class="jssocials-share-count"></span></div>
                        </div>
                        <div class="jssocials-share jssocials-share-linkedin"><a href="#" class="jssocials-share-link"><i class="fa fa-linkedin-square"></i></a>
                            <div class="jssocials-share-count-box jssocials-share-no-count"><span class="jssocials-share-count"></span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--SECTION 10-->

    <!--SECTION 11-->
    <section class="content-center-no-border-section11" id="section-11-web">
        <div class="container">
            <div class="row d-flex justify-content-start justify-content-md-center  align-items-center">
                <div class="col-md-12 col-lg-8 text-center ">
                    <div class="box-content-section11">
                        <h3 class="title-content-section-11-title d-flex align-items-center">Perlu <span style="font-weight:700;"> BANTUAN? </span> Hubungi Kami : <span style="font-weight:700;"> 0274-5305505 <span></h3>
                    </div>
                </div>
                <div class="col-md-12 col-lg-4 text-md-center">
                    <button class="button-blue-list-white"><i class="fa fa-comments pr-lg-2"></i><span>Live Chat<span></button>
                </div>
            </div>
        </div>
    </section>

    <section class="content-center-no-border-section11" id="section-11-ipad">
        <div class="container">
            <div class="row d-flex align-items-center">
                <div class="col-md-12 ">
                    <div class="box-content-section11 text-center">
                        <h3 class="title-content-section-11-title d-flex justify-content-center align-items-center">Perlu <span style="font-weight:700;"> BANTUAN? </span> Hubungi Kami : </h3>
                        <h3 class="title-content-section-11-title d-flex justify-content-center align-items-center"><span style="font-weight:700;"> 0274-5305505 </span></h3>
                        <button class="button-blue-list-white-mobile "><i class="fa fa-comments pr-lg-2"></i><span>Live Chat</span></button>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content-center-no-border-section11" id="section-11-mobile">
        <div class="container">
            <div class="row d-flex align-items-center">
                <div class="col-md-12 col-lg-8">
                    <div class="box-content-section11 text-center">
                        <h3 class="title-content-section-11-title d-flex justify-content-center align-items-center">Perlu <span style="font-weight:700;"> BANTUAN? </span></h3>
                        <h3 class="title-content-section-11-title d-flex justify-content-center align-items-center">Hubungi Kami : </h3>
                        <h3 class="title-content-section-11-title d-flex justify-content-center align-items-center"><span style="font-weight:700;"> 0274-5305505 </span></h3>
                        <button class="button-blue-list-white-mobile"><i class="fa fa-comments pr-lg-2"></i><span>Live Chat</span></button>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
    <!--SECTION 11-->

    <!--SECTION 12-->
    <footer>
        <div class="content-center-no-border-section12" id="section12-web">
            <div class="container">
                <div class="row d-flex justify-content-start ">
                    <div class="col-md-3 col-sm-12 text-left ">
                        <div class="title-content-section12">
                            <h5>HUBUNGI KAMI</h5>
                        </div>
                        <div class="content-content-section12">
                            <p>0274-5305505</p>
                            <p>Senin - Minggu</p>
                            <p>24 Jam Nonstop</p>
                            <br>
                            <p>Jl. Selokan Mataram Monjali</p>
                            <p>Karangjati MT I/304</p>
                            <p>Sinduadi, Mlati, Sleman</p>
                            <p>Yogyakarta 552284</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <div class="title-content-section12 text-left">
                            <h5>LAYANAN</h5>
                        </div>
                        <div class="content-content-section12">
                            <p><a class="footer-links" href="">Domain</a></p>
                            <p><a class="footer-links" href="">Shared Hosting</a></p>
                            <p><a class="footer-links" href="">Cloud VPS Hosting</a></p>
                            <p><a class="footer-links" href="">Managed VPS Hosting</a></p>
                            <p><a class="footer-links" href="">Web Builder</a></p>
                            <p><a class="footer-links" href="">Keamanan SSL/HTTPS</a></p>
                            <p><a class="footer-links" href="">Jasa Pembuatan Website</a></p>
                            <p><a class="footer-links" href="">Program Affiliasi</a></p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <div class="title-content-section12 text-left">
                            <h5>SERVICE HOSTING</h5>
                        </div>
                        <div class="content-content-section12">
                            <p><a class="footer-links" href="">Hosting Murah</a></p>
                            <p><a class="footer-links" href="">Hosting Indonesia</a></p>
                            <p><a class="footer-links" href="">Hosting Singapore SG</a></p>
                            <p><a class="footer-links" href="">Hosting WordPress</a></p>
                            <p><a class="footer-links" href="">Email Hosting</a></p>
                            <p><a class="footer-links" href="">Niagahoster Partner</a></p>
                            <p><a class="footer-links" href="">Web Hosting Unlimited</a></p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <div class="title-content-section12 text-left">
                            <h5>TUTORIAL</h5>
                        </div>
                        <div class="content-content-section12">
                            <p><a class="footer-links" href="">Ebook Gratis</a></p>
                            <p><a class="footer-links" href="">Knowledgebase</a></p>
                            <p><a class="footer-links" href="">Blog</a></p>
                            <p><a class="footer-links" href="">Cara Pembayaran</a></p>
                        </div>
                    </div>
                </div>
                <div class="row d-flex justify-content-start ">
                    <div class="col-md-3 col-sm-12 text-left ">
                        <div class="title-content-section12">
                            <h5>TENTANG KAMI</h5>
                        </div>
                        <div class="content-content-section12">
                            <p><a class="footer-links" href="">Penawaran & Promo Spesial</a></p>
                            <p><a class="footer-links" href="">Niaga Poin</a></p>
                            <p><a class="footer-links" href="">Karir</a></p>
                            <p><a class="footer-links" href="">Kontak Kami</a></p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <div class="title-content-section12 text-left">
                            <h5>KENAPA PILIH NIAGAHOSTER?</h5>
                        </div>
                        <div class="content-content-section12">
                            <p><a class="footer-links" href="">Hosting Terbaik</a></p>
                            <p><a class="footer-links" href="">Datacenter Hosting Terbaik</a></p>
                            <p><a class="footer-links" href="">Domain Gratis</a></p>
                            <p><a class="footer-links" href="">Bagi-bagi Domain Gratis</a></p>
                            <p><a class="footer-links" href="">Bagi-bagi Hosting Gratis<< /a>
                            </p>
                            <p><a class="footer-links" href="">Review Pelanggan</a></p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <div class="title-content-section12 text-left">
                            <h5>NEWSLETTER</h5>
                        </div>
                        <div class="content-newsletter-section12">
                            <input id="samplees" class="form-control email_newsletter" name="email" value="" placeholder="Email" type="text">
                            <span class="input-group-btn">
                                <button class="btn btn-info btn-newsletter" type="submit">Berlangganan</button>
                            </span>
                            <p class="title-content-newsletter">Dapatkan promo dan konten menarik dari penyedia <a href="">web hosting</a> Anda.</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12 d-flex justify-content-center">
                        <div class="content-social-media-section12 ">
                            <button class="social-media-footer-button"><i class="fa fa-facebook"></i></button>
                            <button class="social-media-footer-button"><i class="fa fa-twitter"></i></button>
                            <button class="social-media-footer-button"><i class="fa fa-instagram"></i></button>
                            <button class="social-media-footer-button"><i class="fa fa-linkedin"></i></button>
                        </div>
                    </div>
                </div>
                <div class="row d-flex justify-content-start ">
                    <div class="col-md-12 col-sm-12 text-left ">
                        <div class="title-content-section12">
                            <h5>Pembayaran</h5>
                        </div>
                        <div class="content-payment-section12">
                            <ul>
                                <li><img src="./assets/logo-bni-bordered.svg" alt=""></li>
                                <li><img src="./assets/logo-bca-bordered.svg" alt=""></li>
                                <li><img src="./assets/bri.svg" alt=""></li>
                                <li><img src="./assets/mandiriclickpay.svg" alt=""></li>
                                <li><img src="./assets/permatabank.svg" alt=""></li>
                                <li><img src="./assets/atmbersama.svg" alt=""></li>
                                <li><img src="./assets/prima.svg" alt=""></li>
                                <li><img src="./assets/alto.svg" alt=""></li>
                                <li><img src="./assets/visa.svg" alt=""></li>
                                <li><img src="./assets/mastercard.svg" alt=""></li>
                                <li><img src="./assets/indomaret.svg" alt=""></li>
                                <li><img src="./assets/paypal.svg" alt=""></li>
                                <li><img src="./assets/logo-gopay.svg" alt=""></li>
                            </ul>
                            <p class="title-content-payment">Aktivasi instan dengan e-Payment. Hosting dan <a href="">domain</a> langsung aktif!</p>
                        </div>
                    </div>
                </div>

                <div class="row d-flex justify-content-center ">
                    <div class="col-md-12 col-sm-12 line-footer-copyright">
                    </div>
                </div>
            </div>

            <div class="row d-flex justify-content-center ">
                <div class="col-md-8 col-sm-12 text-center ">
                    <div class="text-copyright">
                        <p>Copyright ©2021 Niagahoster | Hosting powered by PHP8, CloudLinux, CloudFlare, BitNinja and <a href="">DC DCI-Indonesia</a> <br> Cloud <a href="">VPS Murah</a> powered by Webuzo Softaculous, Intel SSD and cloud computing technology</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 text-center">
                    <div class="text-copyright">
                        <a href="">Syarat dan Ketentuan</a>|
                        <a href="">Kebijakan Privasi</a>
                    </div>
                </div>

            </div>
        </div>
        </div>



        <div class="content-center-no-border-section12" id="section12-ipad">
            <div class="container">
                <div class="row d-flex justify-content-start ">
                    <div class="col-md-3 col-sm-3 text-left ">
                        <div class="title-content-section12">
                            <h5>HUBUNGI KAMI</h5>
                        </div>
                        <div class="content-content-section12">
                            <p>0274-5305505</p>
                            <p>Senin - Minggu</p>
                            <p>24 Jam Nonstop</p>
                            <br>
                            <p>Jl. Selokan Mataram Monjali</p>
                            <p>Karangjati MT I/304</p>
                            <p>Sinduadi, Mlati, Sleman</p>
                            <p>Yogyakarta 552284</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <div class="title-content-section12 text-left">
                            <h5>LAYANAN</h5>
                        </div>
                        <div class="content-content-section12">
                            <p><a class="footer-links" href="">Domain</a></p>
                            <p><a class="footer-links" href="">Shared Hosting</a></p>
                            <p><a class="footer-links" href="">Cloud VPS Hosting</a></p>
                            <p><a class="footer-links" href="">Managed VPS Hosting</a></p>
                            <p><a class="footer-links" href="">Web Builder</a></p>
                            <p><a class="footer-links" href="">Keamanan SSL/HTTPS</a></p>
                            <p><a class="footer-links" href="">Jasa Pembuatan Website</a></p>
                            <p><a class="footer-links" href="">Program Affiliasi</a></p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <div class="title-content-section12 text-left">
                            <h5>SERVICE HOSTING</h5>
                        </div>
                        <div class="content-content-section12">
                            <p><a class="footer-links" href="">Hosting Murah</a></p>
                            <p><a class="footer-links" href="">Hosting Indonesia</a></p>
                            <p><a class="footer-links" href="">Hosting Singapore SG</a></p>
                            <p><a class="footer-links" href="">Hosting WordPress</a></p>
                            <p><a class="footer-links" href="">Email Hosting</a></p>
                            <p><a class="footer-links" href="">Niagahoster Partner</a></p>
                            <p><a class="footer-links" href="">Web Hosting Unlimited</a></p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <div class="title-content-section12 text-left">
                            <h5>TUTORIAL</h5>
                        </div>
                        <div class="content-content-section12">
                            <p><a class="footer-links" href="">Ebook Gratis</a></p>
                            <p><a class="footer-links" href="">Knowledgebase</a></p>
                            <p><a class="footer-links" href="">Blog</a></p>
                            <p><a class="footer-links" href="">Cara Pembayaran</a></p>
                        </div>
                    </div>
                </div>
                <div class="row d-flex justify-content-start ">
                    <div class="col-md-3 col-sm-12 text-left ">
                        <div class="title-content-section12">
                            <h5>TENTANG KAMI</h5>
                        </div>
                        <div class="content-content-section12">
                            <p><a class="footer-links" href="">Penawaran & Promo Spesial</a></p>
                            <p><a class="footer-links" href="">Niaga Poin</a></p>
                            <p><a class="footer-links" href="">Karir</a></p>
                            <p><a class="footer-links" href="">Kontak Kami</a></p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <div class="title-content-section12 text-left">
                            <h5>KENAPA PILIH NIAGAHOSTER?</h5>
                        </div>
                        <div class="content-content-section12">
                            <p><a class="footer-links" href="">Hosting Terbaik</a></p>
                            <p><a class="footer-links" href="">Datacenter Hosting Terbaik</a></p>
                            <p><a class="footer-links" href="">Domain Gratis</a></p>
                            <p><a class="footer-links" href="">Bagi-bagi Domain Gratis</a></p>
                            <p><a class="footer-links" href="">Bagi-bagi Hosting Gratis<< /a>
                            </p>
                            <p><a class="footer-links" href="">Review Pelanggan</a></p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12">

                    </div>
                    <!--<div class="col-md-3 col-sm-12 d-flex justify-content-center">
                        <div class="content-social-media-section12 ">
                            <button class="social-media-footer-button"><i class="fa fa-facebook"></i></button>
                            <button class="social-media-footer-button"><i class="fa fa-twitter"></i></button>
                            <button class="social-media-footer-button"><i class="fa fa-instagram"></i></button>
                            <button class="social-media-footer-button"><i class="fa fa-linkedin"></i></button>
                        </div>
                    </div> -->
                </div>

                <div class="row">

                    <div class="col-sm-6 col-xs-6">
                        <div class="title-content-section12">
                            <h5>Pembayaran</h5>
                        </div>
                        <div class="row content-payment-section12">
                            <ul>
                                <li class="col-sm-4 col-xs-4 col-xss-4"><img src="./assets/logo-bni-bordered.svg" alt=""></li>
                                <li class="col-sm-4 col-xs-4 col-xss-4"><img src="./assets/logo-bca-bordered.svg" alt=""></li>
                                <li class="col-sm-4 col-xs-4 col-xss-4"><img src="./assets/mandiriclickpay.svg" alt=""></li>
                            </ul>
                        </div>

                        <div class="row content-payment-section12 ">
                            <ul>
                                <li class="col-sm-4 col-xs-4 col-xss-4"><img src="./assets/permatabank.svg" alt=""></li>
                                <li class="col-sm-4 col-xs-4 col-xss-4"><img src="./assets/atmbersama.svg" alt=""></li>
                                <li class="col-sm-4 col-xs-4 col-xss-4"><img src="./assets/prima.svg" alt=""></li>
                            </ul>
                        </div>

                        <div class="row content-payment-section12 ">
                            <ul>
                                <li class="col-sm-4 col-xs-4 col-xss-4"><img src="./assets/alto.svg" alt=""></li>
                                <li class="col-sm-4 col-xs-4 col-xss-4"><img src="./assets/visa.svg" alt=""></li>
                                <li class="col-sm-4 col-xs-4 col-xss-4"><img src="./assets/mastercard.svg" alt=""></li>
                            </ul>
                        </div>

                        <p class="title-content-payment text-left pt-5">Aktivasi instan dengan e-Payment. <br>Hosting dan <a href="">domain</a> langsung aktif!</p>


                    </div>
                    <div class="col-sm-6 col-xs-6">
                        <div class="title-content-section12 text-left">
                            <h5>NEWSLETTER</h5>
                        </div>
                        <div class="content-newsletter-section12">
                            <input id="samplees" class="form-control email_newsletter" name="email" value="" placeholder="Email" type="text">
                            <span class="input-group-btn">
                                <button class="btn btn-info btn-newsletter" type="submit">Berlangganan</button>
                            </span>
                            <p class="title-content-newsletter">Dapatkan promo dan konten menarik dari penyedia <a href="">web hosting</a> Anda.</p>
                        </div>
                    </div>

                    <div class="col-sm-12 col-xs-12 pt-3">
                        <div class="row content-payment-section12 d-flex justify-content-center">
                            <ul>
                                <li class="col-sm-3 col-xs-4 col-xss-4"><button class="social-media-footer-button"><i class="fa fa-facebook"></i></button></li>
                                <li class="col-sm-3 col-xs-4 col-xss-4"><button class="social-media-footer-button"><i class="fa fa-twitter"></i></button></li>
                                <li class="col-sm-3 col-xs-4 col-xss-4"><button class="social-media-footer-button"><i class="fa fa-instagram"></i></button></li>
                                <li class="col-sm-3 col-xs-4 col-xss-4"><button class="social-media-footer-button"><i class="fa fa-linkedin"></i></button></li>
                            </ul>
                        </div>
                    </div>
                </div>




                <div class="row d-flex justify-content-center ">
                    <div class="col-md-12 col-sm-12 line-footer-copyright">
                    </div>
                </div>
            </div>

            <div class="row d-flex justify-content-center ">
                <div class="col-md-12 col-sm-12 col-xs-12 text-center ">
                    <div class="text-copyright">
                        <p>Copyright ©2021 Niagahoster | Hosting powered by PHP8, CloudLinux, CloudFlare, BitNinja and <a href="">DC DCI-Indonesia</a> <br> Cloud <a href="">VPS Murah</a> powered by Webuzo Softaculous, Intel SSD and cloud computing technology</p>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                    <div class="text-copyright">
                        <a href="">Syarat dan Ketentuan</a>|
                        <a href="">Kebijakan Privasi</a>
                    </div>
                </div>

            </div>
        </div>
        </div>

        <div class="content-center-no-border-section12" id="section12-ipad2">
            <div class="container">
                <div class="row d-flex justify-content-start ">
                    <div class="col-md-3 col-sm-3 text-left ">
                        <div class="title-content-section12">
                            <h5>HUBUNGI KAMI</h5>
                        </div>
                        <div class="content-content-section12">
                            <p>0274-5305505</p>
                            <p>Senin - Minggu</p>
                            <p>24 Jam Nonstop</p>
                            <br>
                            <p>Jl. Selokan Mataram Monjali</p>
                            <p>Karangjati MT I/304</p>
                            <p>Sinduadi, Mlati, Sleman</p>
                            <p>Yogyakarta 552284</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <div class="title-content-section12 text-left">
                            <h5>LAYANAN</h5>
                        </div>
                        <div class="content-content-section12">
                            <p><a class="footer-links" href="">Domain</a></p>
                            <p><a class="footer-links" href="">Shared Hosting</a></p>
                            <p><a class="footer-links" href="">Cloud VPS Hosting</a></p>
                            <p><a class="footer-links" href="">Managed VPS Hosting</a></p>
                            <p><a class="footer-links" href="">Web Builder</a></p>
                            <p><a class="footer-links" href="">Keamanan SSL/HTTPS</a></p>
                            <p><a class="footer-links" href="">Jasa Pembuatan Website</a></p>
                            <p><a class="footer-links" href="">Program Affiliasi</a></p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <div class="title-content-section12 text-left">
                            <h5>SERVICE HOSTING</h5>
                        </div>
                        <div class="content-content-section12">
                            <p><a class="footer-links" href="">Hosting Murah</a></p>
                            <p><a class="footer-links" href="">Hosting Indonesia</a></p>
                            <p><a class="footer-links" href="">Hosting Singapore SG</a></p>
                            <p><a class="footer-links" href="">Hosting WordPress</a></p>
                            <p><a class="footer-links" href="">Email Hosting</a></p>
                            <p><a class="footer-links" href="">Niagahoster Partner</a></p>
                            <p><a class="footer-links" href="">Web Hosting Unlimited</a></p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <div class="title-content-section12 text-left">
                            <h5>TUTORIAL</h5>
                        </div>
                        <div class="content-content-section12">
                            <p><a class="footer-links" href="">Ebook Gratis</a></p>
                            <p><a class="footer-links" href="">Knowledgebase</a></p>
                            <p><a class="footer-links" href="">Blog</a></p>
                            <p><a class="footer-links" href="">Cara Pembayaran</a></p>
                        </div>
                    </div>
                </div>
                <div class="row d-flex justify-content-start ">
                    <div class="col-md-3 col-sm-12 text-left ">
                        <div class="title-content-section12">
                            <h5>TENTANG KAMI</h5>
                        </div>
                        <div class="content-content-section12">
                            <p><a class="footer-links" href="">Penawaran & Promo Spesial</a></p>
                            <p><a class="footer-links" href="">Niaga Poin</a></p>
                            <p><a class="footer-links" href="">Karir</a></p>
                            <p><a class="footer-links" href="">Kontak Kami</a></p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <div class="title-content-section12 text-left">
                            <h5>KENAPA PILIH NIAGAHOSTER?</h5>
                        </div>
                        <div class="content-content-section12">
                            <p><a class="footer-links" href="">Hosting Terbaik</a></p>
                            <p><a class="footer-links" href="">Datacenter Hosting Terbaik</a></p>
                            <p><a class="footer-links" href="">Domain Gratis</a></p>
                            <p><a class="footer-links" href="">Bagi-bagi Domain Gratis</a></p>
                            <p><a class="footer-links" href="">Bagi-bagi Hosting Gratis<< /a>
                            </p>
                            <p><a class="footer-links" href="">Review Pelanggan</a></p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <div class="title-content-section12 text-left">
                            <h5>NEWSLETTER</h5>
                        </div>
                        <div class="content-newsletter-section12">
                            <input id="samplees" class="form-control email_newsletter" name="email" value="" placeholder="Email" type="text">
                            <span class="input-group-btn">
                                <button class="btn btn-info btn-newsletter" type="submit">Berlangganan</button>
                            </span>
                            <p class="title-content-newsletter">Dapatkan promo dan konten menarik dari penyedia <a href="">web hosting</a> Anda.</p>
                        </div>
                    </div>
                    <!--<div class="col-md-3 col-sm-12 d-flex justify-content-center">
                        <div class="content-social-media-section12 ">
                            <button class="social-media-footer-button"><i class="fa fa-facebook"></i></button>
                            <button class="social-media-footer-button"><i class="fa fa-twitter"></i></button>
                            <button class="social-media-footer-button"><i class="fa fa-instagram"></i></button>
                            <button class="social-media-footer-button"><i class="fa fa-linkedin"></i></button>
                        </div>
                    </div> -->
                </div>

                <div class="row">

                    <div class="col-md-6 col-sm-6 col-xs-6 col-xxs-6">
                        <div class="title-content-section12">
                            <h5>Pembayaran</h5>
                        </div>
                        <div class="row content-payment-section12">
                            <ul>
                                <li class="col-sm-4 col-xs-4 col-xss-4"><img src="./assets/logo-bni-bordered.svg" alt=""></li>
                                <li class="col-sm-4 col-xs-4 col-xss-4"><img src="./assets/logo-bca-bordered.svg" alt=""></li>
                                <li class="col-sm-4 col-xs-4 col-xss-4"><img src="./assets/bri.svg" alt=""></li>
                                <li class="col-sm-4 col-xs-4 col-xss-4"><img src="./assets/mandiriclickpay.svg" alt=""></li>
                            </ul>
                        </div>

                        <div class="row content-payment-section12 ">
                            <ul>
                                <li class="col-sm-4 col-xs-4 col-xss-4"><img src="./assets/permatabank.svg" alt=""></li>
                                <li class="col-sm-4 col-xs-4 col-xss-4"><img src="./assets/atmbersama.svg" alt=""></li>
                                <li class="col-sm-4 col-xs-4 col-xss-4"><img src="./assets/prima.svg" alt=""></li>
                                <li class="col-sm-4 col-xs-4 col-xss-4"><img src="./assets/alto.svg" alt=""></li>
                            </ul>
                        </div>

                        <div class="row content-payment-section12 ">
                            <ul>
                                <li class="col-sm-4 col-xs-4 col-xss-4"><img src="./assets/visa.svg" alt=""></li>
                                <li class="col-sm-4 col-xs-4 col-xss-4"><img src="./assets/mastercard.svg" alt=""></li>
                                <li class="col-sm-4 col-xs-4 col-xss-4"><img src="./assets/indomaret.svg" alt=""></li>
                                <li class="col-sm-4 col-xs-4 col-xss-4"><img src="./assets/paypal.svg" alt=""></li>
                            </ul>
                        </div>

                        <div class="row content-payment-section12 ">
                            <ul>
                                <li class="col-sm-4 col-xs-4 col-xss-4"><img src="./assets/logo-gopay.svg" alt=""></li>
                            </ul>
                        </div>


                        <p class="title-content-payment text-left">Aktivasi instan dengan e-Payment. <br>Hosting dan <a href="">domain</a> langsung aktif!</p>


                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6 col-xxs-6 pt-5">
                        <div class="row content-payment-section12 d-flex justify-content-center">
                            <ul>
                                <li class="col-sm-4 col-xs-4 col-xss-4"><button class="social-media-footer-button"><i class="fa fa-facebook"></i></button></li>
                                <li class="col-sm-4 col-xs-4 col-xss-4"><button class="social-media-footer-button"><i class="fa fa-twitter"></i></button></li>
                                <li class="col-sm-4 col-xs-4 col-xss-4"><button class="social-media-footer-button"><i class="fa fa-instagram"></i></button></li>
                                <li class="col-sm-4 col-xs-4 col-xss-4"><button class="social-media-footer-button"><i class="fa fa-linkedin"></i></button></li>
                            </ul>
                        </div>
                    </div>
                </div>




                <div class="row d-flex justify-content-center ">
                    <div class="col-md-12 col-sm-12 line-footer-copyright">
                    </div>
                </div>
            </div>

            <div class="row d-flex justify-content-center ">
                <div class="col-md-8 col-sm-12 text-center ">
                    <div class="text-copyright">
                        <p>Copyright ©2021 Niagahoster | Hosting powered by PHP8, CloudLinux, CloudFlare, BitNinja and <a href="">DC DCI-Indonesia</a> <br> Cloud <a href="">VPS Murah</a> powered by Webuzo Softaculous, Intel SSD and cloud computing technology</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 text-center">
                    <div class="text-copyright">
                        <a href="">Syarat dan Ketentuan</a>|
                        <a href="">Kebijakan Privasi</a>
                    </div>
                </div>

            </div>
        </div>
        </div>

        <div class="content-center-no-border-section12" id="section12-mobile">
            <div class="container">
                <div class="row d-flex justify-content-center ">
                    <div class="col-md-3 col-sm-12 text-center ">
                        <div class="title-content-section12">
                            <h5>HUBUNGI KAMI</h5>
                        </div>
                        <div class="content-content-section12">
                            <p>0274-5305505</p>
                            <p>Senin - Minggu</p>
                            <p>24 Jam Nonstop</p>
                            <br>
                            <p>Jl. Selokan Mataram Monjali</p>
                            <p>Karangjati MT I/304</p>
                            <p>Sinduadi, Mlati, Sleman</p>
                            <p>Yogyakarta 552284</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <div class="title-content-section12 text-center">
                            <h5>LAYANAN</h5>
                        </div>
                        <div class="content-content-section12 text-center">
                            <p><a class="footer-links" href="">Domain</a></p>
                            <p><a class="footer-links" href="">Shared Hosting</a></p>
                            <p><a class="footer-links" href="">Cloud VPS Hosting</a></p>
                            <p><a class="footer-links" href="">Managed VPS Hosting</a></p>
                            <p><a class="footer-links" href="">Web Builder</a></p>
                            <p><a class="footer-links" href="">Keamanan SSL/HTTPS</a></p>
                            <p><a class="footer-links" href="">Jasa Pembuatan Website</a></p>
                            <p><a class="footer-links" href="">Program Affiliasi</a></p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <div class="title-content-section12 text-center">
                            <h5>SERVICE HOSTING</h5>
                        </div>
                        <div class="content-content-section12 text-center">
                            <p><a class="footer-links" href="">Hosting Murah</a></p>
                            <p><a class="footer-links" href="">Hosting Indonesia</a></p>
                            <p><a class="footer-links" href="">Hosting Singapore SG</a></p>
                            <p><a class="footer-links" href="">Hosting WordPress</a></p>
                            <p><a class="footer-links" href="">Email Hosting</a></p>
                            <p><a class="footer-links" href="">Niagahoster Partner</a></p>
                            <p><a class="footer-links" href="">Web Hosting Unlimited</a></p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <div class="title-content-section12 text-center">
                            <h5>TUTORIAL</h5>
                        </div>
                        <div class="content-content-section12 text-center">
                            <p><a class="footer-links" href="">Ebook Gratis</a></p>
                            <p><a class="footer-links" href="">Knowledgebase</a></p>
                            <p><a class="footer-links" href="">Blog</a></p>
                            <p><a class="footer-links" href="">Cara Pembayaran</a></p>
                        </div>
                    </div>
                </div>
                <div class="row d-flex justify-content-start ">
                    <div class="col-md-3 col-sm-12 text-center ">
                        <div class="title-content-section12">
                            <h5>TENTANG KAMI</h5>
                        </div>
                        <div class="content-content-section12 text-center">
                            <p><a class="footer-links" href="">Penawaran & Promo Spesial</a></p>
                            <p><a class="footer-links" href="">Niaga Poin</a></p>
                            <p><a class="footer-links" href="">Karir</a></p>
                            <p><a class="footer-links" href="">Kontak Kami</a></p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <div class="title-content-section12 text-center">
                            <h5>KENAPA PILIH NIAGAHOSTER?</h5>
                        </div>
                        <div class="content-content-section12 text-center">
                            <p><a class="footer-links" href="">Hosting Terbaik</a></p>
                            <p><a class="footer-links" href="">Datacenter Hosting Terbaik</a></p>
                            <p><a class="footer-links" href="">Domain Gratis</a></p>
                            <p><a class="footer-links" href="">Bagi-bagi Domain Gratis</a></p>
                            <p><a class="footer-links" href="">Bagi-bagi Hosting Gratis<< /a>
                            </p>
                            <p><a class="footer-links" href="">Review Pelanggan</a></p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <div class="title-content-section12 text-center">
                            <h5>NEWSLETTER</h5>
                        </div>
                        <div class="content-newsletter-section12">
                            <input id="samplees" class="form-control email_newsletter" name="email" value="" placeholder="Email" type="text">
                            <span class="input-group-btn">
                                <button class="btn btn-info btn-newsletter" type="submit">Berlangganan</button>
                            </span>
                            <p class="title-content-newsletter text-center">Dapatkan promo dan konten menarik dari penyedia <a href="">web hosting</a> Anda.</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12 d-flex justify-content-center">
                        <div class="content-social-media-section12 ">
                            <button class="social-media-footer-button"><i class="fa fa-facebook"></i></button>
                            <button class="social-media-footer-button"><i class="fa fa-twitter"></i></button>
                            <button class="social-media-footer-button"><i class="fa fa-instagram"></i></button>
                            <button class="social-media-footer-button"><i class="fa fa-linkedin"></i></button>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 col-xxs-12">
                    <div class="title-content-section12">
                        <h5>Pembayaran</h5>
                    </div>
                    <div class="row content-payment-section12">
                        <ul>
                            <li class="col-sm-4 col-xs-4 col-xss-4"><img src="./assets/logo-bni-bordered.svg" alt=""></li>
                            <li class="col-sm-4 col-xs-4 col-xss-4"><img src="./assets/logo-bca-bordered.svg" alt=""></li>
                            <li class="col-sm-4 col-xs-4 col-xss-4"><img src="./assets/bri.svg" alt=""></li>
                            <li class="col-sm-4 col-xs-4 col-xss-4"><img src="./assets/mandiriclickpay.svg" alt=""></li>
                        </ul>
                    </div>

                    <div class="row content-payment-section12 ">
                        <ul>
                            <li class="col-sm-4 col-xs-4 col-xss-4"><img src="./assets/permatabank.svg" alt=""></li>
                            <li class="col-sm-4 col-xs-4 col-xss-4"><img src="./assets/atmbersama.svg" alt=""></li>
                            <li class="col-sm-4 col-xs-4 col-xss-4"><img src="./assets/prima.svg" alt=""></li>
                            <li class="col-sm-4 col-xs-4 col-xss-4"><img src="./assets/alto.svg" alt=""></li>
                        </ul>
                    </div>

                    <div class="row content-payment-section12 ">
                        <ul>
                            <li class="col-sm-4 col-xs-4 col-xss-4"><img src="./assets/visa.svg" alt=""></li>
                            <li class="col-sm-4 col-xs-4 col-xss-4"><img src="./assets/mastercard.svg" alt=""></li>
                            <li class="col-sm-4 col-xs-4 col-xss-4"><img src="./assets/indomaret.svg" alt=""></li>
                            <li class="col-sm-4 col-xs-4 col-xss-4"><img src="./assets/paypal.svg" alt=""></li>
                        </ul>
                    </div>

                    <div class="row content-payment-section12 ">
                        <ul>
                            <li class="col-sm-4 col-xs-4 col-xss-4"><img src="./assets/logo-gopay.svg" alt=""></li>
                        </ul>
                    </div>
                </div>



                <div class="row">
                    <p class="title-content-payment text-center">Aktivasi instan dengan e-Payment. Hosting dan <a href="">domain</a> langsung aktif!</p>
                </div>

                <div class="row d-flex justify-content-center ">
                    <div class="col-md-12 col-sm-12 line-footer-copyright">
                    </div>
                </div>
            </div>

            <div class="row d-flex justify-content-center ">
                <div class="col-md-8 col-sm-12 text-center ">
                    <div class="text-copyright">
                        <p>Copyright ©2021 Niagahoster | Hosting powered by PHP8, CloudLinux, CloudFlare, BitNinja and <a href="">DC DCI-Indonesia</a> <br> Cloud <a href="">VPS Murah</a> powered by Webuzo Softaculous, Intel SSD and cloud computing technology</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 text-center">
                    <div class="text-copyright">
                        <a href="">Syarat dan Ketentuan</a>|
                        <a href="">Kebijakan Privasi</a>
                    </div>
                </div>

            </div>
        </div>
        </div>
    </footer>
    <!--SECTION 12-->


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script>
        $(document).ready(function(){
               
                $('#nav-btn').click(function(e) {
                    e.preventDefault();
                    if($('#dropdown-nav').css('display')=="block"){
                        $("#dropdown-nav").slideUp();
                        $("#dropdown-inside-menu").slideUp();
                    }else{
                        $("#dropdown-nav").slideDown();
                    }
                });

                $('#dropdown-inside').click(function(e) {
                    e.preventDefault();
                    if($('#dropdown-inside-menu').css('display')=="block"){
                        $("#dropdown-inside-menu").slideUp();
                    }else{
                        $("#dropdown-inside-menu").slideDown();
                    }
                });
        });
    </script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>