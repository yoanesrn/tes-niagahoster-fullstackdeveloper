CREATE TABLE package (
    id int,
    name VARCHAR(255),
    price INT(11),
    price_discount INT(19)
);

INSERT INTO package(id, name,price,price_discount) VALUES
(1,'Bayi','19000','14900'),
(2,'Pelajar','46900','23450'),
(3,'Personal','58000','38900'),
(4,'Bisnis','109900','65900');